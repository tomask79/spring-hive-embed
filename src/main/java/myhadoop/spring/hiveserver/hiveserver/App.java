package myhadoop.spring.hiveserver.hiveserver;

import myhadoop.hiveserver.test.TestBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Demo showing howto run embed hive thrift server 
 * inside of the application context!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext ctx = new 
        		ClassPathXmlApplicationContext("applicationContext.xml");
        
        TestBean testBean = (TestBean) ctx.getBean("testBean");
        
        System.out.println("....Getting databases...");
        for (String db: testBean.getDbs()) {
        	System.out.println("Dbname: "+db);
        }
    }
}
