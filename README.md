# Running the Hive Thrift server inside of Spring application context #

I already showed that it is possible to run Apache Hive 2 server outside of Spring application context, which is preferable option when going into production. But for testing or developing purposes, Spring Hadoop integration also allows you to **run Hive server inside of Spring application context**. 

**How Spring does it:**

- Derby DB is used as metadata storage.
- You need to provide URL where you've got Hadoop HDFS.

(check /hiveserver/src/main/resources/applicationContext.xml file)

**Howto run demo:**
Simply find and run App class. If compiled everything correctly you should see this output:


```
....Getting databases...
Dbname: default
```